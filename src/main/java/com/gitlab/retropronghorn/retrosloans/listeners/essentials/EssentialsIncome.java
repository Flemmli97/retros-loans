package com.gitlab.retropronghorn.retrosloans.listeners.essentials;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosloans.utils.Numbers;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import net.ess3.api.events.UserBalanceUpdateEvent;

public class EssentialsIncome implements Listener {
    private final RetrosLoans instance;

    public EssentialsIncome(RetrosLoans instance) {
        this.instance = instance;
    }

    @EventHandler
    public void playerDepositEvent(UserBalanceUpdateEvent event) {
        Player player = event.getPlayer();
        Borrower borrower = instance.getBorrower(player.getUniqueId());
        Double amount = event.getNewBalance().doubleValue() - event.getOldBalance().doubleValue();
        if (borrower.hasActiveLoan() && amount > 0) {
            // TODO: In a very rare edge case a player could be gifted
            // exactly their loan balance and avoid payments, let's find
            // a better way to handle this in the future.
            System.out.println("Amount " + amount);
            if (amount != borrower.getLoan().getAmount()) {
                double incomeAfterDeductions = borrower.deductFromPayment(amount);
                Double toWithdraw = amount - incomeAfterDeductions;

                instance.getEconomy().withdrawPlayer(
                    (OfflinePlayer) player,
                    player.getWorld().getName(),
                    toWithdraw
                );
                Messenger.sendPlayerMessage(
                    player,
                    Language.format(
                        Language.PAYMENT_DEDUCTED,
                        Numbers.formatDouble(toWithdraw)
                    )
                );
            }
        }
    }

}