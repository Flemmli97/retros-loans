package com.gitlab.retropronghorn.retrosloans.constants;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;

import net.md_5.bungee.api.ChatColor;

public enum Language {
    NAME("main.name"),
    PAYMENT_DEDUCTED("main.payment-deducted"),
    RECEIVED_LOAN("main.received-loan"),
    INVALID_LOAN("main.invalid-loan"),
    NOT_ELIGABLE("main.not-eligable"),
    OUTSTANDING_LOAN("main.outstanding-loan"),
    COOLDOWN("main.cooldown"),
    BALANCE("main.balance"),
    PAYMENT_FAILED("main.payment-failed"),
    CANNOT_AFFORD("main.cannot-afford"),
    LOAN_PAID("main.loan-paid"),
    SPECIFY_LOAN("commands.specify-loan"),
    INVALID_NUMBER("commands.invalid-number"),
    NO_LOAN("commands.no-loan"),
    TITLE_LOAN_AQUIRED("titles.loan-aquired.title"),
    SUBTITLE_LOAN_AQUIRED("titles.loan-aquired.subtitle"),
    TITLE_LOAN_PAID_OFF("titles.paid-off-loan.title"),
    SUBTITLE_LOAN_PAID_OFF("titles.paid-off-loan.subtitle");


    private final String message;

    private Language(String message) {
        this.message = message;
    }

    public String get() {
        return message;
    }

    public static String format(Language lang, String... args) {
        String formattedMessage = RetrosLoans.getInstance()
            .getLanguage()
            .getString(lang.get());
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                String replace = "${val"+(i+1)+"}";
                formattedMessage = formattedMessage.replace(replace.toString(), args[i] + ChatColor.RESET);
            }
        }
        return formattedMessage;
    }
}