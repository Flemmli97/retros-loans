package com.gitlab.retropronghorn.retrosloans.commands;

import java.text.NumberFormat;


import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosloans.utils.Numbers;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Balance {
    private RetrosLoans instance;

    /**
     * Constructs a new balance handler
     * @param instance reference to the main plugin instance
     */
    public Balance(RetrosLoans instance) {
        this.instance = instance;
    }

    /**
     * Executes command to retreive the unpaid balance on
     * a loan for the given command sender
     * @param sender sender of the command
     */
    public void execute(CommandSender sender) {
        Borrower borrower = instance.getBorrower((Player) sender);
        if (borrower.hasActiveLoan()) {
            Double amount = borrower.getLoan().getBalance();
            Messenger.sendPlayerMessage(
                (Player) sender,
                Language.format(
                    Language.BALANCE,
                    ChatColor.RED + Numbers.formatDouble(amount)
                )
            );
        } else {
            Messenger.sendPlayerMessage(
                (Player) sender,
                Language.format(Language.NO_LOAN)
            );
        }
    }
}
