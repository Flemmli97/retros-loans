package com.gitlab.retropronghorn.retrosloans.commands;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.models.Loan;
import com.gitlab.retropronghorn.retrosloans.utils.Numbers;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Info {
    private RetrosLoans instance;

    public Info(RetrosLoans instance) {
        this.instance = instance;
    }

    public void execute(CommandSender sender) {
        sender.sendMessage("--------" +  ChatColor.GREEN + "" + ChatColor.BOLD + "Current Loan" + ChatColor.RESET + "--------");
        sender.sendMessage(" ");

        Loan currentLoan = instance.getBorrower((Player) sender).getLoan();
        String name = "Unknown";
        for (String key : instance.getLoanKeys()) {
            if (instance.getConfig().getDouble("loans."+key+".amount") == currentLoan.getAmount()) {
                name = key;
            }
        }
        sender.sendMessage(
            "name:     " +
            ChatColor.BOLD +
            name
        );
        sender.sendMessage(
            "value:    " +
            ChatColor.GREEN +
            "$" +
            currentLoan.getAmount()
        );
        sender.sendMessage(
            "balance: " +
            ChatColor.RED +
            "$" +
            Numbers.formatDouble(currentLoan.getBalance())
        );
        sender.sendMessage(" ");
        TextComponent message = new TextComponent("[Make Payment]");
        message.setClickEvent(
            new ClickEvent(
                ClickEvent.Action.SUGGEST_COMMAND,
                "/loan pay "
            )
        );
        message.setHoverEvent(
            new HoverEvent(
                HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("Click to Pay").create()
            )
        );
        sender.spigot().sendMessage(message);
    }
}