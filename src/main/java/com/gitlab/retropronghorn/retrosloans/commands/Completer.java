package com.gitlab.retropronghorn.retrosloans.commands;

import java.util.ArrayList;
import java.util.List;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Permissions;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

public class Completer implements TabCompleter {
    private RetrosLoans instance;

    public Completer(RetrosLoans instance) {
        this.instance = instance;
    }

    /**
     * Trims commands list to matching currenlty inputed text
     * @param lst commands list to filter
     * @param query query entered so far
     * @return returns a filtered list of commands
     */
    private List<String> trimListToMatches(List<String> lst, String query) {
        List<String> newList = new ArrayList<String>();
        lst.forEach(item -> {
            if (StringUtils.containsIgnoreCase(item.toString(), query)) {
                newList.add(item.toString());
            }
        });
        return newList;
    }


    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> commands = new ArrayList<String>();
        if (args.length == 1) {
            commands.add("help");

            if (Permissions.hasPermission(Permissions.TAKE, sender)) {
                commands.add("take");
                commands.add("list");
            }

            if (Permissions.hasPermission(Permissions.PAY, sender)) {
                commands.add("pay");
            }

            if (Permissions.hasPermission(Permissions.BALANCE, sender)) {
                commands.add("balance");
            }

            if (Permissions.hasPermission(Permissions.COOLDOWN, sender)) {
                commands.add("cooldown");
            }

            if (Permissions.hasPermission(Permissions.GUI, sender)) {
                commands.add("gui");
            }
            return trimListToMatches(commands, args[0]);
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("take") && Permissions.hasPermission(Permissions.TAKE, sender)) {
                commands.addAll(instance.getLoanKeys());
            }

            if (args[0].equalsIgnoreCase("pay") && Permissions.hasPermission(Permissions.PAY, sender)) {
                commands.add("0");
            }

            return trimListToMatches(commands, args[1]);
        }
        return commands;
    }
}