package com.gitlab.retropronghorn.retrosloans.commands;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Permissions;

import org.bukkit.command.CommandSender;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Help {
    public static void execute(CommandSender sender) {
        sender.sendMessage("--------" +  ChatColor.GREEN + "" + ChatColor.BOLD + "Retro's Loans" + ChatColor.RESET + "--------");
        sender.sendMessage("       " + ChatColor.GRAY + "version " + RetrosLoans.getInstance().getDescription().getVersion());
        sender.sendMessage(" ");

        if (Permissions.hasPermission(Permissions.TAKE, sender)) {
            sender.sendMessage(ChatColor.BOLD + "Take a loan:");
            TextComponent takeLoan = new TextComponent(ChatColor.GRAY + "- /loan take <name>");
            takeLoan.setClickEvent(
                new ClickEvent(
                    ClickEvent.Action.SUGGEST_COMMAND,
                    "/loan take "
                )
            );
            takeLoan.setHoverEvent(
                new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Click to use").create()
                )
            );
            sender.spigot().sendMessage(takeLoan);
        }
        if (Permissions.hasPermission(Permissions.PAY, sender)) {
            sender.sendMessage(ChatColor.BOLD + "Pay a loan:");
            TextComponent payLoan = new TextComponent(ChatColor.GRAY + "- /loan pay <amount>");
            payLoan.setClickEvent(
                new ClickEvent(
                    ClickEvent.Action.SUGGEST_COMMAND,
                    "/loan pay "
                )
            );
            payLoan.setHoverEvent(
                new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Click to use").create()
                )
            );
            sender.spigot().sendMessage(payLoan);
        }
        if (Permissions.hasPermission(Permissions.BALANCE, sender)) {
            sender.sendMessage(ChatColor.BOLD + "Check due balance:");
            TextComponent checkBalance = new TextComponent(ChatColor.GRAY + "- /loan balance");
            checkBalance.setClickEvent(
                new ClickEvent(
                    ClickEvent.Action.SUGGEST_COMMAND,
                    "/loan balance"
                )
            );
            checkBalance.setHoverEvent(
                new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Click to use").create()
                )
            );
            sender.spigot().sendMessage(checkBalance);
        }
        if (Permissions.hasPermission(Permissions.COOLDOWN, sender)) {
            sender.sendMessage(ChatColor.BOLD + "Check loan cooldown:");
            TextComponent checkCooldown = new TextComponent(ChatColor.GRAY + "- /loan cooldown");
            checkCooldown.setClickEvent(
                new ClickEvent(
                    ClickEvent.Action.SUGGEST_COMMAND,
                    "/loan cooldown"
                )
            );
            checkCooldown.setHoverEvent(
                new HoverEvent(
                    HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("Click to use").create()
                )
            );
            sender.spigot().sendMessage(checkCooldown);
        }

        sender.sendMessage(" ");
        sender.sendMessage(ChatColor.GRAY + "You can click a command to autofill.");
    }
}