package com.gitlab.retropronghorn.retrosloans.commands;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class OpenGUI {
    private RetrosLoans instance;

    public OpenGUI(RetrosLoans instance) {
        this.instance = instance;
    }

    public void execute(CommandSender sender) {
        Player player = (Player) sender;
        player.openInventory(
            instance.getGUI()
                .getTailoredInventory(
                    (Player) sender
                )
            );
    }
}