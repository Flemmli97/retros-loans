package com.gitlab.retropronghorn.retrosloans.commands;

import java.time.Instant;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosloans.utils.Time;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Cooldown {
    private RetrosLoans instance;

    public Cooldown(RetrosLoans instance) {
        this.instance = instance;
    }

    /**
     * Sends player the cooldown timer until they can take another loan
     * @param sender the player requesting cooldown time
     */
    public void execute(CommandSender sender) {
        Borrower borrower = instance.getBorrower((Player) sender);
        if (!borrower.hasActiveLoan() && borrower.hasTakenLoan()) {
            Long timeSinceLastLoan = Instant.now().toEpochMilli() - instance.getBorrower((Player) sender).getlastLoanIssuedAt();
            Long cooldownRemaining = instance.getConfig().getLong("cooldown") - timeSinceLastLoan;
            if (timeSinceLastLoan != 0L) {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(
                        Language.COOLDOWN,
                        ChatColor.AQUA + Time.getDurationBreakdown(cooldownRemaining)
                    )
                );
            } else {
                Messenger.sendPlayerMessage(
                    (Player) sender,
                    Language.format(
                        Language.NO_LOAN
                    )
                );
            }
        } else {
            Messenger.sendPlayerMessage(
                (Player) sender,
                Language.format(Language.OUTSTANDING_LOAN)
            );
        }
    }
}
