package com.gitlab.retropronghorn.retrosloans.gui.listeners;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.commands.Executor;
import com.gitlab.retropronghorn.retrosloans.commands.Help;
import com.gitlab.retropronghorn.retrosloans.gui.GUI;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {
    private RetrosLoans instance;
    private Executor executor;

    public InventoryListener(RetrosLoans instance) {
        this.instance = instance;
        this.executor = new Executor(instance);
    }

    public Boolean checkMatches(Inventory a, Inventory b) {
        // TODO: Make this better
        if (a.getSize() != b.getSize()) return false;
        if (a.getItem(0) == null) return false;
        if (a.getItem(0).getItemMeta() == null) return false;
        if (b.getItem(0) == null) return false;
        if (b.getItem(0).getItemMeta() == null) return false;

        return a.getItem(0).getItemMeta().equals(b.getItem(0).getItemMeta());
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        GUI gui = instance.getGUI();
        if (event.getCurrentItem() == null) return;
        if (event.getCurrentItem().getItemMeta() == null) return;
        ItemStack clickedItem = event.getCurrentItem();

        if (checkMatches(event.getInventory(), gui.getTailoredInventory(player))) {
            if (clickedItem.getType() == gui.getHelpItem().getType()) {
                Help.execute((CommandSender) player);
                player.closeInventory();
            }
            if (clickedItem.getType() == gui.getCooldownItem().getType()) {
                executor.cooldown.execute((CommandSender) player);
                player.closeInventory();
            }
            if (clickedItem.getType() == gui.getBalanceItem().getType()) {
                executor.balance.execute((CommandSender) player);
                player.closeInventory();
            }
            if (clickedItem.getType() == gui.getInfoItem().getType()) {
                executor.info.execute((CommandSender) player);
                player.closeInventory();
            }
            if (clickedItem.getType() == gui.getLoansItem().getType()) {
                player.closeInventory();
                player.openInventory(gui.getLoansInventory());
            }
        } else if (checkMatches(event.getInventory(), gui.getLoansInventory())) {
            String key = clickedItem.getItemMeta().getDisplayName().toLowerCase();
            player.closeInventory();
            executor.takeLoan.execute((CommandSender) player, key);
        } else {
            return;
        }

        // ----------------------
        // Cancel UI Events
        // ----------------------
        event.setCancelled(true);
        // Update inv to help prevent spam obtaining items.
        Bukkit.getScheduler().runTaskLater(RetrosLoans.getInstance(), () -> {
            player.updateInventory();
        }, 1L);
    }
}