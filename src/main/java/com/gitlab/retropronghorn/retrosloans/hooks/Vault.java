package com.gitlab.retropronghorn.retrosloans.hooks;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosutils.server.ULogger;

import org.bukkit.plugin.RegisteredServiceProvider;

import net.milkbowl.vault.economy.Economy;

public class Vault {
    private final RetrosLoans instance;
    private final Economy econ;

    /**
     * Create a new hook to vault economy
     * @param instance reference to main plugin instance
     */
    public Vault(RetrosLoans instance) {
        this.instance = instance;
        econ = init();
    }

    /**
     * Get the economy api
     * @return returns economy api
     */
    public Economy get() {
        return econ;
    }

    /**
     * Check that we have an economy plugin on the server
     * if we don't we'll unload the plugin and send an error
     * because the plugin cannont function without one.
     * @return Returns the economy provider
     */
    public Economy init() {
        if (hasEco()) {
            RegisteredServiceProvider<Economy> rsp = instance.getServer()
                .getServicesManager()
                .getRegistration(Economy.class);
            ULogger.info(instance.getDescription().getName(), "Hooked Vault.");
            return rsp.getProvider();
        } else {
            ULogger.error(
                instance.getDescription().getName(),
                "Disabled due to no Vault dependency found!"
            );
            instance.getServer()
                .getPluginManager()
                .disablePlugin(instance);
            return null;
        }
    }

    /**
     * Check if an economy plugin is added to the server
     * @return returns wether or not this server has an economy plugin
     */
    public boolean hasEco() {
        if (instance.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = instance.getServer()
            .getServicesManager()
            .getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        Economy econ = rsp.getProvider();
        return econ != null;
    }
}