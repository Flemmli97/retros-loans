package com.gitlab.retropronghorn.retrosloans.hooks;

import org.bukkit.event.EventHandler;

import net.citizensnpcs.api.trait.Trait;

//This is your trait that will be applied to a npc using the /trait mytraitname command. Each NPC gets its own instance of this class.
//the Trait class has a reference to the attached NPC class through the protected field 'npc' or getNPC().
//The Trait class also implements Listener so you can add EventHandlers directly to your trait.
public class Citizens extends Trait {
	public Citizens() {
        super("lender");
	}

    // An example event handler. All traits will be registered automatically as Bukkit Listeners.
	@EventHandler
	public void click(net.citizensnpcs.api.event.NPCRightClickEvent event){
		//Handle a click on a NPC. The event has a getNPC() method.
		//Be sure to check event.getNPC() == this.getNPC() so you only handle clicks on this NPC!
		if (event.getNPC() == this.getNPC()) {
            System.err.println("Lender clicked!");
        }
	}
}