package com.gitlab.retropronghorn.retrosloans.utils;

import java.text.NumberFormat;

public class Numbers {
    private static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    public static String formatDouble(double value) {
        return NumberFormat.getInstance()
            .format(
                roundAvoid(value, 2)
            );

    }
}