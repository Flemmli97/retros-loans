package com.gitlab.retropronghorn.retrosloans.utils;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Messenger {

    /**
     * Format a message to fit the plugin layout
     * @param message message to format
     * @return returns formatted message
     */
    private static String formatMessage(String message) {
        return "[" + ChatColor.GREEN + "Loans" + ChatColor.RESET + "] " + message;
    }

    /**
     * Sends a player a message formatted to the plugins layout
     * @param player player to message
     * @param message message to send player
     */
    public static void sendPlayerMessage(Player player, String message) {
        player.sendMessage(formatMessage(message));
    }

    /**
     * Sends a player a title message with subtitle text
     * @param player player to display title message to
     * @param title title text
     * @param subtitle subtitle text
     */
    public static void sendPlayerTitle(Player player, String title, String subtitle) {
        player.sendTitle(title, subtitle, 10, 70, 20);
    }
}