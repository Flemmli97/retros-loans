package com.gitlab.retropronghorn.retrosloans.api;

import java.util.HashMap;
import java.util.UUID;

import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.models.Broker;

import org.bukkit.configuration.ConfigurationSection;

public interface Loans {
    /**
     * Get a hashmap of borrowers
     * A borrower is a finanical wrapper of a
     * player and holds active loan data
     * @return returns all borrowers matched to their UUID
     */
    HashMap<UUID, Borrower> getBorrowers();

    /**
     * Get a specific borrower by uuid
     * A borrower represents a finanical wrapper of
     * a player and holds the active loan
     * @param uuid unique id of the borrower
     * @return returns a specific borrower
     */
    Borrower getBorrower(UUID uuid);

    /**
     * Get the miminum possible loan
     * this can be useful when determining if a player can
     * take out a higher loan, you may want them to have taken
     * and repaid the cheapest loan first
     * @return returns configuration selection for the minimum loan value
     */
    ConfigurationSection getMimimumLoan();

    /**
     * Get the broker handling loans
     * the broker handles issuing new loans
     * taking payments and issuing funds
     * @return returns broker
     */
    Broker getBroker();

    Long getBorrowerCooldown(UUID uuid);
}