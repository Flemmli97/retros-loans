package com.gitlab.retropronghorn.retrosloans.api;

import java.util.HashMap;
import java.util.UUID;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.models.Borrower;
import com.gitlab.retropronghorn.retrosloans.models.Broker;

import org.bukkit.configuration.ConfigurationSection;

public abstract class AbstractLoans implements Loans {
    @Override
    public Borrower getBorrower(UUID uuid) {
        return RetrosLoans.getInstance().getBorrower(uuid);
    }

    @Override
    public HashMap<UUID, Borrower> getBorrowers() {
        return RetrosLoans.getInstance().getBorrowers();
    }

    @Override
    public ConfigurationSection getMimimumLoan() {
        return RetrosLoans.getInstance().getMinimumLoan();
    }

    @Override
    public Broker getBroker() {
        return RetrosLoans.getInstance().getBroker();
    }
}

/*
                                                       ,:
                                                    ,' |
                                                  /   :
                                              --'   /
                                              / />/
                                            / /_\
                                        __/   /
                                       )'-. /
                                     ./  :\
                                   /.' '
                                 '/'
                                 +
                                '
                               `.
                            .-"-
                           (    |
                        . .-'  '.
                     ( (.   )8:
                     .'    / (_  )
                       _. :(.   )8P  `
                      .  (  `-' (  `.   .
                    .  :  (   .a8a)
                      /_`( "a `a. )"'
                  (  (/  .  ' )=='
                 (   (    )  .8"   +
                   (`'8a.( _(   (
                ..-. `8P    ) `  )  +
          -'   (      -ab:  )
            '    _  `    (8P"Ya
      _(    (    )b  -`.  ) +
      ( 8)  ( _.aP" _a   \( \   *
    +  )/    (8P   (88    )  )
      (a:f   "     `"       `
    Liftoff of the falcon 9 and crew go nasa,
    go spacex, good luck Bob and Doug!
    America has launched! - May 30, 2020
*/