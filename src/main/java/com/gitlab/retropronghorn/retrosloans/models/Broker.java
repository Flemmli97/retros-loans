package com.gitlab.retropronghorn.retrosloans.models;

import java.text.NumberFormat;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.constants.Language;
import com.gitlab.retropronghorn.retrosloans.events.LoanIssued;
import com.gitlab.retropronghorn.retrosloans.utils.Messenger;
import com.gitlab.retropronghorn.retrosutils.sound.USound;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.economy.Economy;

public class Broker {
    private final RetrosLoans instance;

    /**
     * Create a new broker linked to main plugin instance
     * @param instance reference to main plugin instance
     */
    public Broker(RetrosLoans instance) {
        this.instance = instance;
    }

     /**
     * Create a new loan for a player
     * @param key loan key to create
     * @param player player to create loan on
     * @return returns newley created loan
     */
    public Loan createLoan(String key, Player player) {
        ConfigurationSection loans = instance.getConfig()
            .getConfigurationSection("loans");
        ConfigurationSection loan = loans.getConfigurationSection(key);
        System.out.println("Created loan " + loan.getDouble("amount") + " " + loan.getDouble("rate"));
        return new Loan(
            instance.getBorrower(player),
            loan.getDouble("amount"),
            loan.getDouble("rate")
        );
    }

    /**
     * Takes payment towards loans from a players income
     * @param income the amount of money the player recived
     * @param player the player receiving a billable payment
     * @return returns any overpayment 0.0 if none.
     */
    public double takePayment(double income, Player player) {
        Borrower borrower = instance.getBorrower(player);
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
        double dedcutedAmount = borrower.calcDeductions(income);
        // Take money from players account to pay towards loan
        instance.getEconomy().withdrawPlayer(
            offlinePlayer,
            player.getWorld().getName(),
            dedcutedAmount
        );
        // Let the player know the money has been taken
        Messenger.sendPlayerMessage(
            player,
            Language.format(
                Language.PAYMENT_DEDUCTED,
                ChatColor.RED + NumberFormat.getInstance().format(dedcutedAmount)
            )
        );
        return borrower.deductFromPayment(income);
    }

    /**
     * Issue a new loan to a player
     * @param key key of loan to issue to borrower
     * @param player player to reference borrower account to
     * @return returns wether or not the loan was issued successfully
     */
    public Boolean issueLoan(String key, Player player) {
        Borrower borrower = instance.getBorrower(player);
        ConfigurationSection loans = instance.getConfig()
            .getConfigurationSection("loans");
        ConfigurationSection loan = loans.getConfigurationSection(key);
        if (borrower.canTakeLoan(loan.getDouble("amount"))) {
            borrower.setLoan(
                createLoan(key, player)
            );
            borrower.getLoan()
                .applyInterest(loan.getDouble("interest"));
            issueFunds(
                borrower.getLoan().getAmount(),
                borrower.getPlayer()
            );
            Bukkit.getServer()
                .getPluginManager()
                .callEvent(
                    new LoanIssued(
                        borrower.getLoan()
                    )
                );
            return true;
        }
        return false;
    }

    /**
     * Send the funds to the player when the loan has been created
     * @param amount amount to send to player
     * @param player player to send loan funds to
     */
    private void issueFunds(double amount, Player player) {
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
        World world = player.getWorld();
        // Let player know the loan was granted
        Messenger.sendPlayerMessage(
            player,
            Language.format(
                Language.RECEIVED_LOAN,
                ChatColor.GREEN + NumberFormat.getInstance().format(amount)
            )
        );
        Messenger.sendPlayerTitle(
            player,
            Language.format(
                Language.TITLE_LOAN_AQUIRED
            ),
            Language.format(
                Language.SUBTITLE_LOAN_AQUIRED,
                NumberFormat.getInstance().format(amount)
            )
        );
        player.playSound(
            player.getLocation(),
            USound.soundOrFallback(instance.getConfig().getString("title-sound")),
            1F,
            1F
        );
        // Send player money
        instance.getEconomy()
            .depositPlayer(offlinePlayer, world.getName(), amount);
    }

    /**
     * Make a payment on behalf of a borrower towards a loan
     * @param player player making the payment
     * @param amount amount they wish to pay
     * @return returns if they payment was successful
     */
    public boolean makePayment(Player player, double amount) {
        Borrower borrower = instance.getBorrower(player);
        Economy economy = instance.getEconomy();
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());

        // If the user tries to make a payment of more money than they have, reject
        if (economy.getBalance(offlinePlayer) < amount) return false;
        // If the user doesn't have an active loan we don't need to go futher
        if (!borrower.hasActiveLoan()) return false;

        // pass the payment along to the borrower and record overpayment
        double overpayment = borrower.pay(amount);
        // Take the paid amount from the player, and return any overpayments
        economy.withdrawPlayer(offlinePlayer, player.getWorld().getName(), amount);
        economy.depositPlayer(offlinePlayer, overpayment);

        if (!borrower.hasActiveLoan()) {
            // Loan paid off
            Messenger.sendPlayerTitle(
                player,
                Language.format(
                    Language.TITLE_LOAN_PAID_OFF
                ),
                Language.format(
                    Language.SUBTITLE_LOAN_PAID_OFF
                )
            );
            player.playSound(
                player.getLocation(),
                USound.soundOrFallback(instance.getConfig().getString("title-sound")),
                1F,
                1F
            );
        }
        return true;
    }

    public double deductFromPayment(double income, Player player) {
        return instance.getBorrower(
            player.getUniqueId()
        ).deductFromPayment(income);
    }
}