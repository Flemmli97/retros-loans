package com.gitlab.retropronghorn.retrosloans.models;

import java.util.UUID;

import com.gitlab.retropronghorn.retrosloans.events.LoanPaidOff;
import com.gitlab.retropronghorn.retrosloans.events.LoanPaymentEvent;

import org.bukkit.Bukkit;

public class Loan {
    private UUID uuid;
    private Borrower borrower;
    private double rate;

    private double amount;
    private double balance;
    private boolean paid = false;

    /**
     * Construct a new loan
     * @param borrower borrower tied to this loan
     * @param amount amount of the loan
     * @param rate deduction rate on payments
     */
    public Loan(Borrower borrower, double amount, double rate) {
        this.borrower = borrower;
        this.amount = amount;
        this.rate = rate;
        this.balance = amount;
        this.uuid = UUID.randomUUID();
    }

    /**
     * Get total loan amount
     * @return returns total loan amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Get the unpaid balance on this loan
     * @return returns the unpaid balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Get the deduction rate of the loan
     * this is the percentage of income taken
     * from payments to the borrower to repay the loan
     * @return returns the deduction rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * Get the borrower tied to this loan
     * @return returns the borrower tied to the loan
     */
    public Borrower getBorrower() {
        return borrower;
    }

    /**
     * Calculate how much overpayment would return after
     * a given payment amount.
     * @param amount payment amount
     * @return returns overpayment if any
     */
    public double calcOverpayment(double amount) {
        return Math.abs(balance - amount);
    }

    /**
     * Check wether the loan is paid off
     * @return returns wether or not the loan is paid off
     */
    public boolean isPaidOff() {
        return paid;
    }

    /**
     * Get the Unique Identifier of the loan
     * @return returns the loan UUID
     */
    public UUID getUniqueId() {
        return uuid;
    }

    public void applyInterest(double rate) {
        this.balance += this.amount * rate;
    }

    public void setBalance(double amount) {
        this.balance = amount;
    }

    public void setIsPaidOff(boolean paidOff) {
        this.paid = paidOff;
    }

    /**
     * Make a payment towards the loan
     * @param amount amount to pay towards the loan
     * @return returns overpayment amount
     */
    public double makePayment(double amount) {
        Bukkit.getPluginManager().callEvent(new LoanPaymentEvent(this, amount));
        // Check if the payment is not more than what's due.
        if (balance - amount >= 0) {
            balance -= amount;
            // Overpayment non existant
            if (balance == 0) {
                paid = true;
                // Fire payment event
                Bukkit.getServer()
                    .getPluginManager()
                    .callEvent(
                        new LoanPaidOff(this)
                    );
            }
            return 0.0;
        }
        // We've payed over the amount due, let the payer know
        double overpayment = Math.abs(balance - amount);
        balance = 0;
        paid = true;
        // Fire payment event
        Bukkit.getServer()
            .getPluginManager()
            .callEvent(
                new LoanPaidOff(this)
            );
        return overpayment;
    }
}