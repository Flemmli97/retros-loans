package com.gitlab.retropronghorn.retrosloans.models;

import java.time.Instant;

import com.gitlab.retropronghorn.retrosloans.RetrosLoans;
import com.gitlab.retropronghorn.retrosloans.events.LoanPaymentEvent;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Borrower {
    private Player borrower;
    private Loan loan;
    private double lifetimePayments = 0;

    private double minimum;
    private double trust;

    private long lastLoanIssuedAt;

    /**
     * Create a new borrower
     * @param player player representing the borrower
     * @param minimum smallest possible loan
     * @param trust percentage of loan borrower must have made in lifetime payments to take new loan (minus minimum)
     */
    public Borrower(Player player, double minimum, double trust) {
        borrower = player;
        this.minimum = minimum;
        this.trust = trust;
    }

    /**
     * Get the miniumum loan this borrower can take
     * @return returns minimum loan amount
     */
    public double getMinimum() {
        return minimum;
    }


    /**
     * Gets the trust required for this borrower
     * @return returns trust ratio
     */
    public double getTrust() {
        return trust;
    }
    /**
     * Get the active loan the borrower has
     * @return returns the last active loan
     */
    public Loan getLoan() {
        return loan;
    }

    /**
     * Get the player class of this borrower
     * @return player class of the borrower
     */
    public Player getPlayer() {
        return borrower;
    }

    /**
     * Get toe total lifetime payments a user has made towards loans
     * this can be a sort of "credit score" to use
     * @return
     */
    public double getLifetimePayments() {
        return lifetimePayments;
    }

    /**
     * Get the last time a loan was issued, this can be useful
     * for setting cooldowns on loans to prevent abuse.
     * @return returns epoch timestamp of last loan
     */
    public long getlastLoanIssuedAt() {
        return lastLoanIssuedAt;
    }

    /**
     * Check if the borrower has had a loan in the past
     * @return returns if borrower has ever taken a loan
     */
    public Boolean hasTakenLoan() {
        return lifetimePayments > 0;
    }

    /**
     * Check wether or not a borrower has an active loan
     * since we can only have one loan at a time this
     * is important to check before issuing a new loan
     * @return wether or not a borrower has active loan
     */
    public Boolean hasActiveLoan() {
        return loan != null &&
            loan.getBalance() > 0;
    }

    /**
     * Calculate the amount that would be deducted from a payment
     * @param amount amount of payment
     * @return returns the amount deducted
     */
    public double calcDeductions(double amount) {
       return amount * loan.getRate();
    }

    /**
     * Set the lifetime payments for this borrower
     * useful when loading the borrower from storage
     * @param amount amount to set lifetime payments to
     */
    public void setLifetimePayments(double amount) {
        this.lifetimePayments = amount;
    }

    /**
     * Set a new loan on the borrowers account
     * @param amount amount of the loan
     * @param rate rate at which the loan deducts from payments
     * @param lastLoanIssuedAt set the time manually for last loan issue date
     * useful when loading from storage
     * @return returns the newley created loan
     */
    public Loan setLoan(double amount, double rate, long lastLoanIssuedAt) {
        setLoan(new Loan(this, amount, rate));
        // Overrides setloans assignment
        this.lastLoanIssuedAt = lastLoanIssuedAt;
        return loan;
    }

    /**
     * Set a new loan on the borrowers account
     * @param amount amount of the loan
     * @param rate rate at which the loan deducts from payments
     * @return returns the newley created loan
     */
    public Loan setLoan(double amount, double rate) {
        setLoan(new Loan(this, amount, rate));
        return loan;
    }

    /**
     * Set the active loan for this borrower
     * @param loan loan to set as the active loan
     */
    public void setLoan(Loan loan) {
        lastLoanIssuedAt = Instant.now().toEpochMilli();
        this.loan = loan;
    }

    /**
     * Check wether a borrower can take a new loan
     * @param amount amount the borrower wishes to borrow
     * @return returns wether or not the borrower may take a new loan
     */
    public Boolean canTakeLoan(double amount) {
        long cooldown = RetrosLoans.getInstance()
            .getConfig()
            .getLong("cooldown");
        // if we have an active loan already, we can't take another
        if (hasActiveLoan()) return false;
        // Prevent abuse, set a cooldown on loan creation
        if (Instant.now().toEpochMilli() - lastLoanIssuedAt < cooldown) return false;
        double loanAfterMinumum = amount - minimum;
        double maximumLoan = lifetimePayments + (lifetimePayments * trust);
        return loanAfterMinumum <= maximumLoan;
    }

    /**
     * Take a new loan
     * @param amount amount to borrow
     * @param rate rate at which the loan deducts from payments
     */
    public void takeLoan(double amount, double rate) {
        if (canTakeLoan(amount))
            setLoan(amount, rate);
    }

    /**
     * Deduct from borrower pay events to repay a loan
     * @param income amount to deduct loan rate from
     * @return new payment after due balance deductions
     */
    public double deductFromPayment(double income) {
        // Deduct from the payment the player would normally recive to
        // the amount after removing the loan rate
        double deductedAmount = income * loan.getRate();
        double incomeAfterdeductions = income - deductedAmount;
        // Increase the players lifetime payments
        lifetimePayments += deductedAmount;
        // Check if we've payed over the balance due
        double overpayment = loan.makePayment(deductedAmount);
        // If we've payed over the due balance add it back to payment
        incomeAfterdeductions += overpayment;
        // Fire payment event
        Bukkit.getServer()
            .getPluginManager()
            .callEvent(
                new LoanPaymentEvent(
                    getLoan(),
                    deductedAmount
                )
            );
        // Return the new payment amount after deductions
        return incomeAfterdeductions;
    }

    /**
     * Make a payment towards the active loan
     * @param amount
     * @return
     */
    public double pay(double amount) {
        if (hasActiveLoan()) {
            double overpayment = getLoan().makePayment(amount);
            // Since we may have overpaid, we will take away the overpayment
            // from the actual amount deducted.
            double deductedAmount = amount - overpayment;
            // Increase lifetime payments
            lifetimePayments += deductedAmount;
            // Fire payment event
            Bukkit.getServer()
            .getPluginManager()
            .callEvent(
                new LoanPaymentEvent(
                    getLoan(),
                    deductedAmount
                )
            );
            return overpayment;
        }
        return 0;
    }
}